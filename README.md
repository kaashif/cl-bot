cl-bot
======

This is a very simple bot that does some limited regex replacement on
your past IRC chat messages.

Usage
-----
Here is what using this bot might look like:

	<myname> hey look at this cool ifle
	<myname> s/ifle/file/me
	<bot> <myname> hey look at this cool file

It's not really a regex, it's a string in the format:
"s/target/replacement/nick". It takes the last message from "nick"
that the bot has seen and replaces "target" with "replacement" in it,
then prints the message out.

License
-------
See `LICENSE`, it's the MIT license.
