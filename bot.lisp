(require :cl-irc)
(require :cl-ppcre)
(in-package :irc)

(defparameter *connection* (connect :nickname "superbot12346969"
									:server "irc.freenode.net"))
(defparameter *chan* "#testing123")
(defparameter *lastmsg* (make-hash-table :test 'equal))

;; Checks if a message is one of th regexes we want
(defun regex-p (message)
  (ppcre:scan "s/(.+)/(.+)/(.+)" message))

;; Does the regex stuff
(defun regex-do (message sender)
  (ppcre:register-groups-bind (from to victim) ("s/(.+)/(.+)/(.+)" message)
	(let* ((realvictim (if (equal victim "me")
						   sender
						   victim))
		   (lastmsg (or (gethash realvictim *lastmsg*) "")))
	  (concatenate 'string "<" realvictim "> " (ppcre:regex-replace-all from lastmsg to)))))

;; This hook is run on every privmsg, checks if we need to do anything
;; then does it if we do
(defun msg-hook (message)
  (let ((msg (format nil "~{~a~^ ~}" (cdr (arguments message)))))
	(if (regex-p msg)
		(privmsg *connection* *chan* (regex-do msg (source message)))
		(setf (gethash (source message) *lastmsg*) msg))))

(defun main ()
  (join *connection* *chan*)
  (add-hook *connection* 'irc-privmsg-message #'msg-hook)
  (read-message-loop *connection*))
